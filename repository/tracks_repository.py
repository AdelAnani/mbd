import pymysql

class TracksRepository:
    def find_all_tracks(self):
        raise NotImplemented("Not Implemented")

    def find_tracks_by_id(self, album_id):
        raise NotImplemented("Not Implemented")

class SqlTracksRepository(TracksRepository):
    def __init__(self, config_dict):
        self.host = config_dict['host']
        self.user = config_dict['user']
        self.password = config_dict['password']
        self.db_name = config_dict['db_name']

    def _get_db_connection(self):
        return pymysql.connect(self.host, self.user, self.password, self.db_name)

    def find_track_by_name(self, track_name):
        connection = self._get_db_connection()
        cursor = connection.cursor()
        cursor.execute("SELECT track.trackId, track.trackName, track.trackDuration,album.albumName  FROM track,albumTrack,album WHERE track.trackId = albumTrack.trackId AND album.albumId = albumTrack.albumId  AND track.trackName = %s", track_name)
        tracks_data_list = cursor.fetchall()
        if len(tracks_data_list)> 0 :
            list_tracks = []
            for row in tracks_data_list:
                list_tracks.append({
                    'id': row[0],
                    'name': row[1],
                    'duration': row[2],
                    'album_name': row[3]
                })
            cursor.close()
            connection.close()
            print(list_tracks)
        else :
            list_tracks = None
        return list_tracks

    def find_all_tracks(self,):
        connection = self._get_db_connection()
        cursor = connection.cursor()
        cursor.execute("SELECT track.trackId, track.trackName, track.trackDuration,album.albumName  FROM track,albumTrack,album WHERE track.trackId = albumTrack.trackId AND album.albumId = albumTrack.albumId ")
        all_tracks = cursor.fetchall()
        list_tracks = []
        for row in all_tracks:
            list_tracks.append({
             'id': row[0],
             'name': row[1],
             'duration': row[2],
             'name_album': row[3]
            })
        cursor.close()
        connection.close()
        return list_tracks

    def find_track_by_id(self, track_id):
        connection = self._get_db_connection()
        cursor = connection.cursor()
        cursor.execute("SELECT track.trackId, track.trackName, track.trackDuration,album.albumName  FROM track,albumTrack,album WHERE track.trackId = albumTrack.trackId AND album.albumId = albumTrack.albumId  AND track.trackId = %s", track_id)
        tracks_data_list = cursor.fetchall()
        list_tracks = []
        for row in tracks_data_list :
            list_tracks.append({
                'id': row[0],
                'name': row[1],
                'duration': row[2],
                'album_name': row[3]
                })
        cursor.close()
        connection.close()
        return list_tracks


