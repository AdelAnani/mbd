import pymysql

class FavoriteRepository:
    def find_all_tracks(self, id_user):
        raise NotImplemented("Not Implemented")

class SqlFavoriteRepository(FavoriteRepository):
    def __init__(self, config_dict):
        self.host = config_dict['host']
        self.user = config_dict['user']
        self.password = config_dict['password']
        self.db_name = config_dict['db_name']

    def _get_db_connection(self):
        return pymysql.connect(self.host, self.user, self.password, self.db_name)

    def find_all_tracks(self,userID):
        connection = self._get_db_connection()
        cursor = connection.cursor()
        cursor.execute (("SELECT * FROM favorite WHERE favorite.userId = %s "),userID)
        all_tracks = cursor.fetchall()
        list_tracks = []
        for row in all_tracks:
            list_tracks.append({
                'id_user': row[0],
                'id_track': row[1],
            })
        cursor.close()
        connection.close()
        return list_tracks




