import pymysql

class ArtistesRepository:
    def find_all_artistes(self):
        raise NotImplemented("Not Implemented")

    def find_artiste_by_id(self, album_id):
        raise NotImplemented("Not Implemented")

class SQLArtistesRepository(ArtistesRepository):

    def __init__(self, config_dict):
        self.host = config_dict['host']
        self.user = config_dict['user']
        self.password = config_dict['password']
        self.db_name = config_dict['db_name']

    def _get_db_connection(self):
        return pymysql.connect(self.host, self.user, self.password, self.db_name)

    def find_artiste_by_name(self, artiste_name):
        connection = self._get_db_connection()
        cursor = connection.cursor()
        cursor.execute("SELECT *  FROM artist, album, artistAlbum WHERE artist.artistName = %s and artist.artistId = artistAlbum.artistId and  album.albumId = artistAlbum.albumId   ", artiste_name )
        artist_data = cursor.fetchall()
        if len(artist_data)> 0:
            artist = {
                'id': artist_data[0][0],
                'name': artist_data[0][1],
                'description': artist_data[0][2],
                'image': artist_data[0][3],
                'albums': []
            }
            for row in artist_data:
                artist['albums'].append({
                    'id': row[4],
                    'name': row[5],
                    'description': row[6],
                    'image': row[7],
                })
            connection.close()
        else :
            artist = None
        return artist

    def find_all_artistes(self):
        connection = self._get_db_connection()
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM artist")
        artistes = cursor.fetchall()
        artistes_list = []
        for row in artistes:
            artistes_list.append({
             'id': row[0],
             'name': row[1],
             'image': row[3],
            })
        cursor.close()
        connection.close()
        return artistes_list

    def find_artiste_by_id(self, artiste_id):
        connection = self._get_db_connection()
        cursor = connection.cursor()
        cursor.execute("SELECT *  FROM artist, album, artistAlbum WHERE artist.artistId = %s and artist.artistId = artistAlbum.artistId and  album.albumId = artistAlbum.albumId   ", artiste_id)
        artist_data_list = cursor.fetchall()
        artist = {
            'id': artist_data_list[0][0],
            'name': artist_data_list[0][1],
            'description': artist_data_list[0][2],
            'image': artist_data_list[0][3],
            'albums': []
        }
        for row in artist_data_list:
            artist['albums'].append({
                'id': row[4],
                'name': row[5],
                'description': row[6],
                'image': row[7],
                'date_release': row[8],
            })
        cursor.close()
        connection.close()
        return artist