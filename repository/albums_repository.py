import pymysql

class AlbumsRepository:
    def find_all_albums(self):
        raise NotImplemented("Not Implemented")

    def find_album_by_id(self, album_id):
        raise NotImplemented("Not Implemented")

class SqlAlbumsRepository(AlbumsRepository):
    def __init__(self, config_dict):
        self.host = config_dict['host']
        self.user = config_dict['user']
        self.password = config_dict['password']
        self.db_name = config_dict['db_name']

    def _get_db_connection(self):
        return pymysql.connect(self.host, self.user, self.password, self.db_name)

    def find_album_by_name (self, album_name):
        connection = self._get_db_connection()
        cursor = connection.cursor()
        cursor.execute("SELECT *  FROM album,albumTrack, track WHERE album.albumId = albumTrack.albumId AND albumTrack.trackId = track.trackId AND album.albumName = %s ", album_name)
        album_data = cursor.fetchall()
        if len(album_data) > 0:
            album = {
                'id': album_data[0][0],
                'name': album_data[0][1],
                'description': album_data[0][2],
                'image': album_data[0][3],
                'dateRelease': album_data[0][4],
                'tracks': []
            }
            for track in album_data:
                album['tracks'].append({
                    'trackId': track[6],
                    'trackName': track[7],
                    'trackDuration': track[8]
                })
            cursor.close()
            connection.close()
        else:
            album = None
        return album

    def find_all_albums(self):
        connection = self._get_db_connection()
        cursor = connection.cursor()
        cursor.execute("SELECT album.albumId, album.albumName, album.albumPhoto, artist.artistName  FROM album,artistAlbum, artist WHERE album.albumId = artistAlbum.albumId AND artistAlbum.artistId = artist.artistId ")
        albums = cursor.fetchall()
        albums_list = []
        for row in albums:
            albums_list.append({
                'id': row[0],
                'name': row[1],
                'image': row[2],
                'artist': row[3],
            })
        cursor.close()
        connection.close()
        return albums_list


    def find_album_by_id(self, album_id):
        connection = self._get_db_connection()
        cursor = connection.cursor()
        cursor.execute("SELECT *  FROM album,albumTrack, track WHERE album.albumId = albumTrack.albumId AND albumTrack.trackId = track.trackId AND album.albumId = %s ", album_id)
        albumData = cursor.fetchall()
        album = {
            'id': albumData[0][0],
            'name': albumData[0][1],
            'description': albumData[0][2],
            'image': albumData[0][3],
            'dateRelease': albumData[0][4],
            'tracks': []
        }
        for track in albumData:
            album['tracks'].append({
                'trackId': track[6],
                'trackName': track[7],
                'trackDuration': track[8]
            })
        cursor.close()
        connection.close()
        return album


