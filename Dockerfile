FROM python:3.7-slim

WORKDIR /

ADD requirements.txt /
RUN pip3 install -r requirements.txt

ADD . /
EXPOSE 8080

CMD ["python3","app.py"]