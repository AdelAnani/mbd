import pymysql
from repository.albums_repository import SqlAlbumsRepository
from repository.artistes_repository import SQLArtistesRepository
from repository.tracks_repository import SqlTracksRepository
from repository.favorite_repository import SqlFavoriteRepository
from service.albums_service import AlbumsService
from service.artistes_service import ArtistesService
from service.favorite_service import FavoriteService
from service.tracks_service import TracksService
from functools import wraps
from passlib.hash import sha256_crypt
from flask import Flask, render_template, flash, redirect, url_for, session, request
from wtforms import Form, StringField, PasswordField, validators

app = Flask(__name__)
app.secret_key = "super secret key"

@app.route('/')
def index():
    return render_template('home.html')

DB_Musify_Users_config = {
    'host': 'db',
    'user': 'root',
    'password': 'root',
    'db_name': 'MusifyUsers'
}

DB_Musify_config = {
    'host': 'db',
    'user': 'root',
    'password': 'root',
    'db_name': 'Musify'
}

album_repository = SqlAlbumsRepository(DB_Musify_config)
albums_service = AlbumsService(album_repository)
artistes_repository = SQLArtistesRepository(DB_Musify_config)
artistes_service = ArtistesService(artistes_repository)
track_repository = SqlTracksRepository(DB_Musify_config)
tracks_service = TracksService(track_repository)
favorite_repository = SqlFavoriteRepository(DB_Musify_config)
favorite_service = FavoriteService(favorite_repository)

def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login', 'danger')
            return redirect(url_for('login'))
    return wrap

class RegisterForm(Form):
    name = StringField('Nom', [validators.length(min=4, max=50)])
    email = StringField('Adresse Courriel', [validators.length(min=4, max=50)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Mots de passe différents')
    ])
    confirm = PasswordField('Confirm Password')

@app.route('/register', methods=['GET','POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        connection = pymysql.connect(DB_Musify_Users_config['host'], DB_Musify_Users_config['user'], DB_Musify_Users_config['password'], DB_Musify_Users_config['db_name'])
        cur = connection.cursor()
        result = cur.execute("SELECT userEmail FROM  user WHERE userEmail = %s",form.email.data)
        connection.commit()
        cur.close()
        if  result > 0 :
             return render_template('register.html', form=form)
        else:
            connection = pymysql.connect(DB_Musify_Users_config['host'], DB_Musify_Users_config['user'], DB_Musify_Users_config['password'],
                                         DB_Musify_Users_config['db_name'])
            cur = connection.cursor()
            cur.execute("INSERT INTO user(userEmail, userPassword, name) VALUES(%s, %s,%s)",
                    (form.email.data, sha256_crypt.encrypt(str(form.password.data)), form.name.data))
            connection.commit()
            cur.close()
            flash('Bienvenue sur Musify', 'success')
            return  redirect(url_for('dashboard'))
    return render_template('register.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        connection = pymysql.connect(DB_Musify_Users_config['host'], DB_Musify_Users_config['user'], DB_Musify_Users_config['password'], DB_Musify_Users_config['db_name'])
        cur = connection.cursor()
        result = cur.execute("SELECT * FROM user WHERE userEmail = %s", [request.form['email']])
        if result > 0:
            data = cur.fetchall()
            password = data[0][2]
            name = data[0][3]
            user_id = data [0][0]
            if sha256_crypt.verify(request.form['password'], password):
                session['logged_in'] = True
                session['user_id'] = user_id
                session['email'] = request.form['email']
                session['name'] = name
                flash('Connexion à Musify', 'success')
                return redirect(url_for('dashboard'))
            else:
                error = 'Données invalides'
                return render_template('login.html', error=error)
            cur.close()
        else:
            error = 'Email invalide'
            return render_template('login.html', error=error)
    return render_template('login.html')

@app.route('/logout')
@is_logged_in
def logout():
    session.clear()
    flash('Vous êtes déconnecté', 'success')
    return redirect(url_for('login'))

@app.route('/dashboard')
@is_logged_in
def dashboard():
    tracks_id = []
    for track in favorite_service.get_all_tracks(session["user_id"]):
        tracks_id.append(track["id_track"])
    track_list = []
    for id in tracks_id:
        track = tracks_service.get_track_by_id(id)
        track_list.append(track[0])
    return render_template('dashboard.html',tracks = track_list)

@app.route('/tracks/add_track/<int:id>')
def add_favorite(id):
    connection = pymysql.connect(DB_Musify_config['host'], DB_Musify_config['user'], DB_Musify_config['password'], DB_Musify_config['db_name'])
    cur = connection.cursor()
    cur.execute("INSERT INTO favorite(userId, trackId) VALUES(%s, %s)", (session["user_id"], id))
    connection.commit()
    cur.close()
    return redirect(url_for('dashboard'))

@app.route('/tracks/delete_track/<int:id>')
def delete_track_from_favorite(id):
    connection = pymysql.connect(DB_Musify_config['host'], DB_Musify_config['user'], DB_Musify_config['password'], DB_Musify_config['db_name'])
    cur = connection.cursor()
    cur.execute("DELETE FROM favorite WHERE userId = %s AND trackId = %s ", (session["user_id"], id))
    connection.commit()
    cur.close()
    return redirect(url_for('dashboard'))

@app.route('/Albums')
@is_logged_in
def find_all_albums():
    return render_template('albums.html', Albums=albums_service.get_all_albums())

@app.route('/albums/<int:id>/')
@is_logged_in
def find_an_album(id):
    try:
        album = albums_service.find_album_by_id(int(id))
        return render_template('album.html', album=album)
    except Exception as e:
        redirect(url_for('find_all_albums'))

@app.route('/albums/search')
@is_logged_in
def search_album_by_term():
    try:
        return render_template('album.html', album=albums_service.find_album_by_name(str(request.args.get('term'))))
    except Exception as e:
       return  redirect(url_for('find_all_albums'))

@app.route('/Artistes')
@is_logged_in
def find_all_artists():
    return render_template('artistes.html', Artistes= artistes_service.get_all_artistes())

@app.route('/Artistes/<int:id>/')
@is_logged_in
def find_an_artist(id):
    try:
        return render_template('artiste.html', artist= artistes_service.find_artiste_by_id(int(id)))
    except:
        return redirect(url_for('find_all_artists'))

@app.route('/Artistes/search')
@is_logged_in
def search_artist_by_term():
    try:
        return render_template('artiste.html', artist= artistes_service.find_artiste_by_name(request.args.get('term')))
    except :
        return redirect(url_for('find_all_artists'))

@app.route('/tracks')
@is_logged_in
def find_all_tracks():
    return render_template('tracks.html', tracks= tracks_service.get_all_tracks())

@app.route('/tracks/<int:id>/')
@is_logged_in
def search_track_by_id(id):
    return render_template('track.html', track= tracks_service.get_track_by_id(id))

@app.route('/tracks/search')
@is_logged_in
def search_track_by_term():
    track_name = request.args.get('term')
    try:
        return render_template('track.html', track=tracks_service.get_track_by_name(track_name))
    except:
        return redirect(url_for('find_all_tracks'))

app.run('0.0.0.0', 8080)

if __name__ == '__main__':
    app.run(debug=True)
