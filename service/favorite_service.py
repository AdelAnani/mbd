class FavoriteService:
    def __init__(self, favorite_repository):
        self.favorite_repository = favorite_repository

    def get_all_tracks(self,user_id):
        return self.favorite_repository.find_all_tracks(user_id)

