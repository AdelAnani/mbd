from repository.albums_repository import AlbumsRepository

class AlbumsService:
    def __init__(self, albums_repository):
        self.albums_repository = albums_repository

    def find_album_by_id(self, id_album):
        album = self.albums_repository.find_album_by_id(id_album)
        if album is None:
            raise ValueError("No Album found")
        return album

    def find_album_by_name(self, name_album):
        album = self.albums_repository.find_album_by_name(name_album)
        if album is None:
            raise ValueError("No Album found")
        return album

    def get_all_albums(self):
        return self.albums_repository.find_all_albums()






