class TracksService:
    def __init__(self, tracks_repository):
        self.tracks_repository = tracks_repository

    def get_track_by_id(self, id_track):
        track = self.tracks_repository.find_track_by_id(id_track)
        if track is None:
            raise ValueError("No Track Found")
        return track

    def get_track_by_name(self, name_track):
        track = self.tracks_repository.find_track_by_name(name_track)
        if track is None:
            raise ValueError("No Track Found")
        return track

    def get_all_tracks(self):
        return self.tracks_repository.find_all_tracks()




