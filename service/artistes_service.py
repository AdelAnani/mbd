from repository.albums_repository import AlbumsRepository


class ArtistesService:
    def __init__(self, artiste_repository):
        self.artiste_repository = artiste_repository

    def find_artiste_by_id(self, artist_id):
        artist = self.artiste_repository.find_artiste_by_id(artist_id)
        if artist is None:
            raise ValueError("No Artist Found")
        return artist

    def find_artiste_by_name(self, artist_name):
        artist = self.artiste_repository.find_artiste_by_name(artist_name)
        if artist is None:
            raise ValueError("No Artist Found")
        return artist

    def get_all_artistes(self):
        return self.artiste_repository.find_all_artistes()





